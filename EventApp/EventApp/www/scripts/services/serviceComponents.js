﻿Globalservices.factory('ComponentsService', function () {
    var components = [{
        componentId: 'forgotPassword',
        step: 1,
        stepName: 'Send',
        inputs: [
                    { type: 'email', name: 'Email', id: 'txtEmail_1', placeholder: 'Email', model: 'email' }
        ]
    },{
        componentId: 'forgotPassword',
        step: 2,
        stepName: 'Validate',
        inputs: [
            { type: 'text', name: 'Code', id: 'txtCode_2', placeholder: 'Code', model: 'code' }
        ]
    }, {
        componentId: 'forgotPassword',
        step: 3,
        stepName: 'Update Password',
        inputs: [
            { type: 'password', name: 'Password', id: 'txtPassword_3', placeholder: 'Password', model: 'password1' },
            { type: 'password', name: 'Password Repeat', id: 'txtPasswordRepeat_3', placeholder: 'Password Repeat', model: 'password2' }
        ]
    }];

    return {
        all: function () {
            return components;
        },
        get: function (componentId, step) {
            return components.filter(function (component) {
                return component.componentId === componentId && component.step === step;
            });
        }
    };
});