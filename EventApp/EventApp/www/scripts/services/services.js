var Globalservices = angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'http://files.softicons.com/download/culture-icons/popular-anime-icons-by-iconspedia/png/256x256/Dragonball-Goku.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Andrew Jostlin',
    lastText: 'Did you get the ice cream?',
    face: 'http://vignette3.wikia.nocookie.net/saintseiya-pedia/images/6/64/Favicon.ico/revision/20150409193744?path-prefix=es'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://scontent-dfw1-1.xx.fbcdn.net/hphotos-xlp1/v/t1.0-9/12509616_10153354871911546_1642095893436988629_n.jpg?_nc_eui=ARgYahWbMGOH_xRimB9y2vKMa8CV2HXRG4pnSvNABvqmdU6UWNJUvdzg7JIq&oh=331c6d056cb751bcd7ee1846bd1a9512&oe=575B2DF1'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
