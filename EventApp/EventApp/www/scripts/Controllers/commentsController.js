﻿//angular.module('starter.controllers', [])

starterControllers.controller('popUpCommentsController', function ($scope, $ionicModal, $ionicPopup) { //$ionicPopup) {

    $scope.modal = $ionicModal.fromTemplate( '<ion-modal-view>' +
      ' <ion-header-bar>' +
         '<h1 class = "title">Modal Title</h1>' +
      '</ion-header-bar>' +
		
      '<ion-content>'+'<button class = "button icon icon-left ion-ios-close-outline" ng-click = "closeModal()">Close Modal</button>' +
      '</ion-content>' +
		
      '</ion-modal-view>', {
         scope: $scope,
         animation: 'slide-in-up'
     })

    $scope.openModal = function() {
        $scope.modal.show();
    };
	
    $scope.closeModal = function() {
        $scope.modal.hide();
    };
	
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });
	
    // Execute action on hide modal
    $scope.$on('modal.hidden', function() {
        // Execute action
    });
	
    // Execute action on remove modal
    $scope.$on('modal.removed', function() {
        // Execute action
    });


/*-----------------------------------------------------------------------------------------------------*/
    $scope.showPopup = function () {
        $scope.data = {}

        // Custom popup
        var myPopup = $ionicPopup.show({
            template: '<input type = "text" ng-model = "data.model">',
            title: 'Title',
            subTitle: 'Subtitle',
            scope: $scope,

            buttons: [
               { text: 'Cancel' }, {
                   text: '<b>Save</b>',
                   type: 'button-positive',
                   onTap: function (e) {

                       if (!$scope.data.model) {
                           //don't allow the user to close unless he enters model...
                           e.preventDefault();
                       } else {
                           return $scope.data.model;
                       }
                   }
               }
            ]
        });

        myPopup.then(function (res) {
            console.log('Tapped!', res);
        });
    };


})


//starterControllers.controller('popUpCommentsController', function ($scope, $ionicPopup) {

//     //When button is clicked, the popup will be shown...
//    $scope.showPopup = function () {
//        $scope.data = {}

//        // Custom popup
//        var myPopup = $ionicPopup.show({
//            template: '<input type = "text" ng-model = "data.model">',
//            title: 'Title',
//            subTitle: 'Subtitle',
//            scope: $scope,

//            buttons: [
//               { text: 'Cancel' }, {
//                   text: '<b>Save</b>',
//                   type: 'button-positive',
//                   onTap: function (e) {

//                       if (!$scope.data.model) {
//                           //don't allow the user to close unless he enters model...
//                           e.preventDefault();
//                       } else {
//                           return $scope.data.model;
//                       }
//                   }
//               }
//            ]
//        });

//        myPopup.then(function (res) {
//            console.log('Tapped!', res);
//        });
//    };
//})