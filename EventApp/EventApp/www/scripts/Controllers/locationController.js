﻿starterControllers
 .controller('MapaCtrl', function ($scope, $ionicLoading, $compile) {

     $scope.$on('$ionicView.enter', function () {
         //function initialize() {
         //$scope.initialize = function(){


         var myLatlng = new google.maps.LatLng(43.07493, -89.381388);

         var mapOptions = {
             center: myLatlng,
             zoom: 16,
             mapTypeId: google.maps.MapTypeId.ROADMAP
         };
         var map = new google.maps.Map(document.getElementById("map"),
             mapOptions);

         //Marker + infowindow + angularjs compiled ng-click
         var contentString = "<div><a ng-click='clickTest()'>Click me!</a></div>";
         var compiled = $compile(contentString)($scope);

         var infowindow = new google.maps.InfoWindow({
             content: compiled[0]
         });

         var marker = new google.maps.Marker({
             position: myLatlng,
             map: map,
             title: 'Uluru (Ayers Rock)'
         });

         google.maps.event.addListener(marker, 'click', function () {
             infowindow.open(map, marker);
         });

         $scope.map = map;

     })

     //google.maps.event.addDomListener(window, 'ready', initialize);
     //window.ionic.Platform.ready(initialize);
     //document.querySelector("#mapasgl").addEventListener(document, 'ready', initialize);

     $scope.centerOnMe = function () {
         if (!$scope.map) {
             return;
         }

         $scope.loading = $ionicLoading.show({
             content: 'Getting current location...',
             showBackdrop: false
         });

         navigator.geolocation.getCurrentPosition(function (pos) {
             $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
             //$scope.loading.hide();
             $ionicLoading.hide();
             //var mimarca = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
             //var marker = google.maps.Marker({
             //    position: mimarca,
             //    map: $scope.map,
             //    title: 'Aqui estoy!'
             //});
             $scope.map.setZoom(32);
             var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
             var beachMarker = new google.maps.Marker({
                 position: { lat: pos.coords.latitude, lng: pos.coords.longitude },
                 map: $scope.map,
                 icon: image
             });

         }, function (error) {
             alert('Unable to get location: ' + error.message);
         });
     };

     $scope.clickTest = function () {
         alert('Example of infowindow with ng-click')
     };


     // onSuccess Callback
     //   This method accepts a `Position` object, which contains
     //   the current GPS coordinates
     //
     function onSuccess(position) {
         //var element = document.getElementById('geolocation');
         //element.innerHTML = 'Latitude: ' + position.coords.latitude + '<br />' +
         //                    'Longitude: ' + position.coords.longitude + '<br />' +
         //                    '<hr />' + element.innerHTML;
         //var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
         //var beachMarker = new google.maps.Marker({
         //    position: { lat: position.coords.latitude, lng: position.coords.longitude },
         //    map: $scope.map,
         //    icon: image
         //});
     }

     // onError Callback receives a PositionError object
     //
     function onError(error) {
         alert('code: ' + error.code + '\n' +
               'message: ' + error.message + '\n');
     }

     // Options: throw an error if no update is received every 30 seconds.
     //
     //var watchID = navigator.geolocation.watchPosition(onSuccess, onError, { timeout: 3000 });

 });