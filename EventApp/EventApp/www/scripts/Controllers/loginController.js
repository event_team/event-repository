﻿// Declaración de controlador
starterControllers.controller('loginCtrl', loginCtrl);

// Inyección de dependencias
loginCtrl.$inject = ['$scope', '$ionicModal', '$rootScope', '$state', '$stateParams', '$translate', '$ionicPopup', '$filter', 'AuthService'];

// Función para el control
function loginCtrl($scope, $ionicModal, $rootScope, $state, $stateParams, $translate, $ionicPopup, $filter, AuthService) {
    var empty = '';

    // Declaración e inicialización de variables del scope
    $scope.messages = [];
    $scope.step = 'send';

    /* Modelos */
    $scope.data = {};
    $scope.register = {};
    $scope.send = {};
    $scope.validate = {};
    $scope.change = {};

    // Metodo de login
    $scope.login = function (data) {
        AuthService.login(data.username, data.password).then(function (authenticated) {
            $state.go('tabIndex.tab.home', {}, { reload: true });
            $scope.setCurrentUsername(data.username);
            $scope.data = {};
        }, function (err) {
            var title = $filter('translate')('LOGIN_POPUP_LOGIN_FAILED_TITLE');
            var text = $filter('translate')('LOGIN_POPUP_LOGIN_FAILED_TEXT');
            $scope.data = {};
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: text,
                okType: 'button-balanced'
            });
        });
    };

    // Declaración del modal Register
    $ionicModal.fromTemplateUrl('templates/register.html', {
        id: 1,
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modalRegister = modal;
    });

    // Declaración del modal Forgot Password
    $ionicModal.fromTemplateUrl('templates/forgotPassword.html', {
        id: 2,
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modalForgotPassword = modal;
    });

    // ########################################### //
    // Metodos para el registro de usuarios        //
    // ########################################### //

    // Abrir modal para registro de usuarios
    $scope.openRegister = function () {
        $scope.modalRegister.show();
    };

    // Cerrar modal para registro de usuarios
    $scope.closeRegister = function () {
        $scope.register = {};
        $scope.messages = [];
        $scope.modalRegister.hide();
    };

    // Metodo registra usuario
    $scope.register = function () {
        $scope.messages = [];

        if ($scope.messages.length === 0) {
            $scope.register = {};
            $scope.closeRegister();
        }
    };

    // ########################################### //
    // Metodos para la recuperación de Contraseñas //
    // ########################################### //

    // Abrir modal para recuperar contraseña
    $scope.openForgotPassword = function () {
        $scope.step = 'send';
        $scope.modalForgotPassword.show();
    };

    // Cierra modal de recuperacion de contraseña
    $scope.closeForgotPassword = function () {
        $scope.messages = [];
        $scope.send = {};
        $scope.validate = {};
        $scope.change = {};
        $scope.modalForgotPassword.hide();
    };

    // Metodo de ejecución de eventos dureante la recuperacion de contraseña
    $scope.onStep = function () {
        switch ($scope.step) {
            case 'send':
                send();
                break;
            case 'validate':
                validate();
                break;
            case 'change':
                change();
                break;
        }
    };

    // Metodo de envió de correo electronico
    var send = function send() {
        if ($scope.send.email) {
            $scope.step = 'validate';
        }
    };

    // Metodo de validación de codigo
    var validate = function validate() {
        if ($scope.validate.code) {
            $scope.step = 'change';
        }
    };

    // Metodo completa proceso de recuperación de contraseña
    var change = function change() {
        $scope.messages = [];

        if ($scope.messages.length === 0) {
            $scope.send = {};
            $scope.validate = {};
            $scope.change = {};
            $scope.closeForgotPassword();
        }
    };
}



