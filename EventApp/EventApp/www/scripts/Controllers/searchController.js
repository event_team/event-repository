﻿starterControllers.controller('SearchCtrl', searchCtrl);

searchCtrl.$inject = ['$scope', 'Searchs', '$translate'];

function searchCtrl($scope, Searchs, $translate) {
    var empty = '';

    // Inicializa Scope
    $scope.searchEvents = [];
    $scope.query = { strQuery: empty };
    $scope.visible = { 'visibility': 'hidden' };

    // Se ejecuta cada vez que se presiona una tecla en el buscador del header
    $scope.keyPressSearch = function () {
        $scope.visible = { 'visibility': 'visible' };

        if ($scope.query.strQuery != empty) {
            $scope.searchEvents = Searchs.get($scope.query.strQuery);
        }
        else {
            $scope.query.strQuery = empty;
            $scope.searchEvents = [];
        }
    }

    // Metodo para cancelar la busqueda
    $scope.cancelSearch = function () {
        $scope.query.strQuery = empty;
        $scope.SearchEvents = Searchs.all();
        $scope.visible = { 'visibility': 'hidden' };
    }
}