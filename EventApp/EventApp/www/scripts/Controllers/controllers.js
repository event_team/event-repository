var starterControllers = angular.module('starter.controllers', [])

starterControllers.controller('MainCtrl', mainCtrl);

mainCtrl.$inject = ['$scope', '$state', '$ionicSideMenuDelegate', '$ionicPopup', '$translate', 'AuthService', 'AUTH_EVENTS'];

function mainCtrl($scope, $state, $ionicSideMenuDelegate, $ionicPopup, $translate, AuthService, AUTH_EVENTS) {
    $scope.username = AuthService.username();

    $scope.toggleLeft = function () {
        $ionicSideMenuDelegate.toggleLeft();
    }

    /* Carga el usuario que inicia sesi�n */
    $scope.setCurrentUsername = function (name) {
        $scope.username = name;
    };
    
    /* Valida que se tengan autorizaci�n para acceder a el recurso */
    $scope.$on(AUTH_EVENTS.notAuthorized, function (event) {
        var title = $filter('translate')('INDEX_LOGIN_POPOUP_UNAUTHORIZED_TITLE');
        var text = $filter('translate')('INDEX_LOGIN_POPOUP_UNAUTHORIZED_TEXT');

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: text,
            okType: 'button-balanced'
        });
    });

    /* Valida que no se pierda la sesi�n */
    $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
        var title = $filter('translate')('INDEX_LOGIN_POPUP_SESSION_LOST_TITLE');
        var text = $filter('translate')('INDEX_LOGIN_POPUP_SESSION_LOST_TEXT');

        AuthService.logout();
        $state.go('tabLogin.login');

        var alertPopup = $ionicPopup.alert({
            title: title,
            template: text,
            okType: 'button-balanced'
        });
    });

    /* Metodo para cerrar sesi�n */
    $scope.logout = function () {
        AuthService.logout();
        $state.go('tabLogin.login');
    };
}


starterControllers
.controller('AccountCtrl', function ($scope) {
    $scope.settings = {
        enableFriends: true
    };
})



