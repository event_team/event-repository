﻿
starterControllers.controller('DatePickerCtrl', function ($scope) {
    $scope.$on('$ionicView.enter', function () {


        var currentDate = new Date();
        var date = new Date(currentDate.getFullYear(), currentDate.getMonth(), 23);
        $scope.date = date;

        $scope.myFunction = function (date) {
            alert(date);
        };

        $scope.onezoneDatepicker = {
            date: date,
            mondayFirst: false,
            //months: ["Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"],
            //daysOfTheWeek: ["Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sa"],
            startDate: new Date(1989, 1, 26),
            endDate: new Date(2024, 1, 26),
            disablePastDays: false,
            disableSwipe: false,
            disableWeekend: false,
            disableDates: [new Date(date.getFullYear(), date.getMonth(), 15), new Date(date.getFullYear(), date.getMonth(), 16), new Date(date.getFullYear(), date.getMonth(), 17)],
            showDatepicker: false,
            showTodayButton: true,
            calendarMode: false,
            hideCancelButton: false,
            hideSetButton: false,
            //callback: $scope.myFunction
        };

        $scope.showDatepicker = function () {
            $scope.onezoneDatepicker.showDatepicker = true;
        };
    });
})