﻿translations =
{
    'es':
    {
        'INDEX_BAR_SEARCH_PLACEHOLDER': 'Buscar',
        
        'INDEX_SIDE_MENU_NEW_EVENT_TEXT': 'Nuevo Evento',
        'INDEX_SIDE_MENU_FRIENDS_TEXT': 'Mi Circulo',
        'INDEX_SIDE_MENU_CONFIG_TEXT': 'Configuración',
        'INDEX_SIDE_MENU_HELP_TEXT': 'Ayuda',
        'INDEX_SIDE_MENU_LOGOUT_TEXT': 'Cerrar Sesión',
        'INDEX_LOGIN_POPOUP_UNAUTHORIZED_TITLE': 'No Autorizado',
        'INDEX_LOGIN_POPOUP_UNAUTHORIZED_TEXT': 'No cuenta con los permisos para acceder a este recurso.',
        'INDEX_LOGIN_POPUP_SESSION_LOST_TITLE': 'La Sesión se ha Perdido',
        'INDEX_LOGIN_POPUP_SESSION_LOST_TEXT': 'Lo sentimos, tiene que volver a iniciar sesión.',

        'LOGIN_USERNAME_PLACEHOLDER': 'Username o Email',
        'LOGIN_PASSWORD_PLACEHOLDER': 'Contraseña',
        'LOGIN_REGISTER_TEXT': 'Registrate',
        'LOGIN_FORGOT_PASSWORD_TEXT': '¿Olvidaste tú contraseña?',
        'LOGIN_LOGIN_BUTTON_TEXT': 'Iniciar Sesión',
        'LOGIN_LOGIN_BUTTON_FACEBOOK_TEXT': 'Iniciar Sesión con Facebook',
        'LOGIN_POPUP_LOGIN_FAILED_TITLE': 'Acceso Denegado',
        'LOGIN_POPUP_LOGIN_FAILED_TEXT': 'Porfavor verifica tus credenciales.',

        'LOGIN_REGISTER_TITLE_BAR': 'Registrate',
        'LOGIN_REGISTER_USERNAME_TEXT': 'Username',
        'LOGIN_REGISTER_USERNAME_PLACEHOLDER': 'Username',
        'LOGIN_REGISTER_EMAIL_TEXT': 'Email',
        'LOGIN_REGISTER_EMAIL_PLACEHOLDER': 'Email',
        'LOGIN_REGISTER_PASSWORD_TEXT': 'Contraseña',
        'LOGIN_REGISTER_PASSWORD_PLACEHOLDER': 'Contraseña',
        'LOGIN_REGISTER_REPEAT_PASSWORD_TEXT': 'Repetir Contraseña',
        'LOGIN_REGISTER_REPEAT_PASSWORD_PLACEHOLDER': 'Repetir Contraseña',
        'LOGIN_REGISTER_BUTTON_TEXT': 'Registrate',

        'LOGIN_FORGOT_PASSWORD_BAR_TITLE': 'Recuperar Contraseña',
        'LOGIN_FORGOT_PASSWORD_EMAIL_TEXT': 'Email',
        'LOGIN_FORGOT_PASSWORD_EMAIL_PLACEHOLDER': 'Email',
        'LOGIN_FORGOT_PASSWORD_SEND_BUTTON_TEXT': 'Enviar a Email',
        'LOGIN_FORGOT_PASSWORD_CODE_TEXT': 'Código',
        'LOGIN_FORGOT_PASSWORD_CODE_PLACEHOLDER': 'Código',
        'LOGIN_FORGOT_PASSWORD_VALIDATE_BUTTON_TEXT': 'Validar Código',
        'LOGIN_FORGOT_PASSWORD_PASSWORD_TEXT': 'Nueva Contraseña',
        'LOGIN_FORGOT_PASSWORD_PASSWORD_PLACEHOLDER': 'Nueva Contraseña',
        'LOGIN_FORGOT_PASSWORD_REPEAT_PASSWORD_TEXT': 'Repetir Contraseña',
        'LOGIN_FORGOT_PASSWORD_REPEAT_PASSWORD_PLACEHOLDER': 'Repetir Contraseña',
        'LOGIN_FORGOT_PASSWORD_CHANGE_BUTTON_TEXT': 'Actualizar Contraseña'

    }

}