(function () {
    'use strict'
    // Ionic Starter App

    // angular.module is a global place for creating, registering and retrieving Angular modules
    // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
    // the 2nd parameter is an array of 'requires'
    // 'starter.services' is found in services.js
    // 'starter.controllers' is found in controllers.js
    angular.module('starter', ['ionic', 'pascalprecht.translate', 'starter.controllers', 'starter.services', 'onezone-datepicker', 'ngMockE2E'])

    .run(function ($ionicPlatform, $rootScope, $state, AuthService, AUTH_EVENTS) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }
        });

        $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
            if ('data' in next && 'authorizedRoles' in next.data) {
                var authorizedRoles = next.data.authorizedRoles;
                if (!AuthService.isAuthorized(authorizedRoles)) {
                    event.preventDefault();
                    $state.go($state.current, {}, { reload: true });
                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                }
            }

            if (!AuthService.isAuthenticated()) {
                if (next.name !== 'tabLogin.login') {
                    event.preventDefault();
                    $state.go('tabLogin.login');
                }
            }
        })
    })

    .run(function ($httpBackend) {
        $httpBackend.whenGET('http://localhost:4400//valid')
        .respond({ message: 'This is my valid response!' });
        $httpBackend.whenGET('http://localhost:4400//notauthenticated')
              .respond(401, { message: "Not Authenticated" });
        $httpBackend.whenGET('http://localhost:4400//notauthorized')
              .respond(403, { message: "Not Authorized" });
        $httpBackend.whenGET(/templates\/\w+.*/).passThrough();
    })

    .config(function ($stateProvider, $urlRouterProvider, $translateProvider, USER_ROLES) {

        // Cargar etiquetas de idiomas
        for (var lang in translations) {
            $translateProvider.translations(lang, translations[lang]);
        }

        // Cargar lenguaje por default
        if (window.localStorage.getItem('lang')) {
            $translateProvider.preferredLanguage(window.localStorage.getItem('lang'));
        } else {
            $translateProvider.preferredLanguage('es');
        };

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

        .state('tabLogin', {
            url: "/tabLogin",
            abstract: true,
            templateUrl: "templates/login.html"
        })

              .state('tabLogin.login', {
                  url: "/login"
              })

        .state('tabIndex', {
            url: "/tabIndex",
            abstract: true,
            templateUrl: "templates/home.html"
        })

              .state('tabIndex.tab', {
                  url: "/tab",
                  abstract: true,
                  templateUrl: "templates/tab-top.html"
              })

                      .state('tabIndex.tab.home', {
                          url: "/home",
                          views: {
                              'tab-dash': {
                                  templateUrl: 'templates/tab-dash.html'
                                  //controller: 'DashCtrl'
                              }
                          }
                      })

                    .state('tabIndex.tab.event-detail', {
                        url: "/home/eventDetail",
                        views: {
                            'tab-dash': {
                                templateUrl: 'templates/event-detail.html',
                                controller: 'EventDetailCtrl'
                            }
                        }
                    })

                      .state('tabIndex.tab.chats', {
                          url: '/chats',
                          views: {
                              'tab-chats': {
                                  templateUrl: 'templates/tab-chats.html',
                                  controller: 'ChatsCtrl'
                              }
                          }
                      })

                      .state('tabIndex.tab.chat-detail', {
                          url: '/chats/:chatId',
                          views: {
                              'tab-chats': {
                                  templateUrl: 'templates/chat-detail.html',
                                  controller: 'ChatDetailCtrl'
                              }
                          }
                      })

                    .state('tabIndex.tab.account', {
                        url: '/account',
                        views: {
                            'tab-account': {
                                templateUrl: 'templates/tab-account.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })



              .state('tabIndex.tab2', {
                  cache: false,
                  url: "/tab2",
                  abstract: true,
                  templateUrl: "templates/tab-bottom.html"
              })

                  .state('tabIndex.tab2.mapa', {
                      cache: false,
                      url: '/mapa',
                      views: {
                          'tab-mapa': {
                              templateUrl: 'templates/tab-mapa.html',
                              controller: 'MapaCtrl'
                          }
                      }
                  })

                  .state('tabIndex.tab2.DatePicker', {
                      cache: false,
                      url: '/DatePicker',
                      views: {
                          'tab-DatePicker': {
                              templateUrl: 'templates/tab-datepicker.html',
                              controller: 'DatePickerCtrl'
                          }
                      }
                  });


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise(function ($injector, $location) {
            var $state = $injector.get("$state");
            $state.go("tabIndex.tab.home");
        });
    })

    .constant('AUTH_EVENTS', {
       notAuthenticated: 'auth-not-authenticated',
       notAuthorized: 'auth-not-authorized'
    })
    .constant('USER_ROLES', {
        admin: 'admin_role',
        public: 'public_role'
    });

})();